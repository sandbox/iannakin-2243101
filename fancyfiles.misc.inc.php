<?php
/**
 * ******************************* HELPER FUNCTIONS
 * **********************************
 */

/**
 * File upload form on view node
 * @param Array $form
 * @param Array $form_state
 * @param StdClass $node
 */
function _fancyfiles_uploadform($form,&$form_state, $node) {
	$form ['#attributes'] = array ( 'class' => 'node-form', 'style' => 'display:none' );
	$form ['file'] = array ('#type' => 'fieldset', '#title' => t ( 'Upload and New Folder Options' ), '#collapsible' => TRUE, '#collapsed' => TRUE, '#description' => t ( 'The "target" folder will receive file uploads, or may be deleted.' ) );

	$target = array ();
	_fancyfiles_recursive_lookup ($node->_filepath, '', $target );
	// targetdir
	$form ['file'] ['targetdir'] = array ('#type' => 'select', '#title' => t ( 'Select a target folder' ), '#options' => $target, 	
			'#description' => t ( 'Choose from the list of opened folders. This menu will update to reflect the folders shown above.' ) );
	$form ['file'] ['filename'] = array ('#type' => 'file', '#title' => t ( 'Upload a file to target directory' ), '#size' => 30, '#description' => t ( 'Click "Browse..." to select a file to upload into the target folder.
			<em>%upload_max maximum file size.</em>', array ('%upload_max' => ini_get ( 'upload_max_filesize' ) ) ) );
	$form ['file'] ['submit'] = array ('#type' => 'submit', '#value' => t ( 'Upload' ) );
	$form ['file'] ['dirname'] = array ('#type' => 'textfield', '#title' => t ( 'New folder name' ), '#description' => t ( 'Enter the folder name you wish to create. New folders will appear in the selected target.' ) );
	$form ['file'] ['create'] = array ('#type' => 'submit', '#value' => t ( 'New folder' ) );
	$form ['file'] ['root'] = array ('#type' => 'hidden', '#value' => $node->_filepath );
	$form ['file'] ['nid'] = array ('#type' => 'hidden', '#value' => $node->nid );
	return $form;
}


/**
 * Recursive lookup for presenting all subfolders inside the upload Form
 * @param string $dir : current directory
 * @param string $virtual_dir : 
 * @param array $res : 
 */
function _fancyfiles_recursive_lookup($dir, $virtual_dir, &$res) {
	if (is_dir ( $dir )) {
		$res [$dir] = ($virtual_dir == '' ? '/' : $virtual_dir);
	}
	$mydir = @opendir ( $dir );
	if (! $mydir) return;
	while ( $f = @readdir ( $mydir ) ) {
		if (is_dir ( $dir . '/' . $f ) && $f != '.' && $f != '..' && $f != '_previews') {
			_fancyfiles_recursive_lookup ( $dir . '/' . $f, $virtual_dir . '/' . $f, $res );
		}
	}
	closedir ( $mydir );
}

/**
 * File upload form validation
 * Checks to ensure file was selected for upload
 * Called by drupal_get_form in _fancyfiles_uploadform()
 */
function _fancyfiles_uploadform_validate($form, &$form_state) {
	switch ($form_state ['values'] ['op']) {
		case t ( 'Upload' ) :
			$upload = $_FILES ['files'] ['name'] ['filename'];
			$error = $_FILES ['files'] ['error'] ['filename'];
			if ($error > 0) {
				$errors = array (1 => "The uploaded file exceeds the " . ini_get ( 'upload_max_filesize' ) . " maximum file size.", 
						2 => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.", 
						3 => "The file was only partially uploaded.", 
						4 => "No file was uploaded.", 
						6 => "The file upload failed, the server is missing it's temporary folder.", 
						7 => "The uploaded file failed to write to disk." );
				form_set_error ( 'filename', t ( $errors [$error] ) );
				break;
			}
			if ($upload == '') {
				form_set_error ( 'filename', t ( 'You must select a file to upload.' ) );
			} else {
				$node = node_load ( $form_state ['values'] ['nid'] );
				if ($node->_whitelist == ''){
					$node->_whitelist = 'jpg, jpeg, gif, png, txt, html, htm, doc, xls, pdf, ppt, pps, ai, psd, zip, rtf, docx, xlsx, pptx, dot, dotx';
				}
				$regex = '/\.(' . preg_replace ( '/[(\s+),]/', '|', preg_quote ( $node->_whitelist ) ) . ')$/i';
				if (! preg_match ( $regex, $upload )) {
					form_set_error ( 'upload', t ( 'The selected file <em>%name</em> can not be attached to this post, because it is only possible to attach files with the following extensions: %files-allowed.', array ('%name' => $upload, '%files-allowed' => $node->_whitelist ) ) );
				}
			}
			break;
		case t ( 'New folder' ) :
			if ($form_state ['values'] ['dirname'] == '') {
				form_set_error ( 'dirname', t ( 'You must first enter a folder name.' ) );
			}
			break;
		default :
			break;
	}

}

/**
 * Saves the file submitted in _fancyfiles_uploadform()
 * Called by drupal_get_form in _fancyfiles_uploadform()
 */
function _fancyfiles_uploadform_submit($form, &$form_state) {
	switch ($form_state ['values'] ['op']) {
		case t ( 'Upload' ) :
			$nid=$form_state ['values'] ['nid'];
			// Get file upload info object
			$file = file_save_upload ( 'filename', array("file_validate_extensions"=>array()), $form_state ['values'] ['targetdir'] );
			if ($file) {
				drupal_chmod ( $form_state ['values'] ['targetdir'] . '/' . $file->filename, 0664 );
				$file->status= FILE_STATUS_PERMANENT;
				file_save($file);
				file_usage_add($file,"fancyfiles","node",$nid);
				drupal_set_message ( 'The file <strong>"' . $file->filename . '"</strong> has successfully been uploaded' );
				$node = node_load ( $nid );
				watchdog ( 'fancyfiles', check_plain ( $file->filename . ' uploaded from ' . $node->title ), NULL, WATCHDOG_NOTICE, l ( t ( 'view' ), 'node/' . $node->nid ) );
			} else {
				drupal_set_message ( 'Upload failed. Please check your filename.', 'error' );
			}
			break;
		case t ( 'New folder' ) :
			$targetdir = $form_state ['values'] ['targetdir'] . '/' . _fancyfiles_sanitize_filename  ( $form_state ['values'] ['dirname'] );
				
			if (! file_prepare_directory ( $targetdir, FILE_CREATE_DIRECTORY )) {
				drupal_set_message ( 'There was a problem creating: <strong>"' . $targetdir . '"</strong>.','error');
			}
			break;

	}

	return;
}

/**
 * Sets up the form for the JAH handler
 * to handle file and folder deletes.
 */
function _fancyfiles_tree_form($form,&$form_state, $node) {
	$form ['delete_uri'] = array ('#type' => 'hidden','#attributes'=>array('id'=>'delete-uri') );
	$form ['nid'] = array ('#type' => 'hidden','#value'=>$node->nid );
	// Javascript handler start settings
	$form ['div'] = array ('#markup' => '<div id="startFancyfiles" ff_path="' . $node->_filepath . '" class="ff_row noDnD droppable" >/</div><div class="container"></div>' );
	return $form;
}

/**
 * Handles the delete file request by submitDelete in fancyfiles.js
 */
function _fancyfiles_tree_form_validate($form, &$form_state) {
	global $user;
	if(!isset($form_state ['values'] ['nid'])) return false;
	$node = node_load ( $form_state ['values'] ['nid'] );
	// ensure that you are only deleting files from the file directory and build
	// path from node
	$delete_uri=  urldecode( $form_state ['values'] ['delete_uri'] );
	// checks user access and confirms that the delete string is within the
	// root, but is not the root itself
	if ((strpos($delete_uri,$node->_filepath) !== false && $delete_uri!==$node->_filepath) && (user_access ( 'modify fancyfiles' ) || (user_access ( 'manage own fancyfiles' ) && ($user->uid == $node->uid)))) {
		if (file_prepare_directory ( $delete_uri )) {
			if (_fancyfiles_recursive_rmdir( $delete_uri )) {
				drupal_set_message ( 'The folder <strong>"' . basename ( $delete_uri ) . '"</strong> and its content has been deleted.' );
				watchdog ( 'fancyfiles', check_plain ( $delete_uri . " and it's contents were deleted from " . $node->title ), NULL, WATCHDOG_NOTICE, l ( t ( 'view' ), 'node/' . $node->nid ) );
			} else {
				drupal_set_message ( 'The folder <strong>"' . basename ( $delete_uri ) . '"</strong> could not be deleted.', 'error' );
				return FALSE;
			}
		}
		else {
			$file= _fancyfiles_fileFromPath($delete_uri);	//gets a File object from the uri
			if ($file && file_delete($file,true)) {
				drupal_set_message ( 'The file <strong>"' . basename ( $delete_uri ) . '"</strong> has been deleted.' );
				watchdog ( 'fancyfiles', check_plain ( $delete_uri . ' was deleted from ' . $node->title ), NULL, WATCHDOG_NOTICE, l ( t ( 'view' ), 'node/' . $node->nid ) );
			} else {
				drupal_set_message ( 'The file <strong>"' . $delete_uri . '"</strong> could not be deleted.', 'error' );
				return FALSE;
			}
		}
	} 
	else {
		drupal_set_message ( 'You are not authorized to remove <strong>"' . $delete_uri . '"</strong>.', 'error' );
	}
	drupal_goto("node/".$node->nid);
}

/**
 * Delete a directory and all it's contents - recursively
 * @param String $filepath : relative path to directory
 */
function _fancyfiles_recursive_rmdir($filepath) {
	// Read directory to delete
	$handle = opendir ( $filepath );
	while ( false !== ($file = readdir ( $handle )) ) {
		if ($file != "." && $file != "..") {
			if (is_dir ( $filepath . '/' . $file )) {
				_fancyfiles_recursive_rmdir ( $filepath . '/' . $file ); // recursive delete directory
			} else {
				$f=_fancyfiles_fileFromPath($filepath . '/' . $file);
				if (!$f || ! file_delete ( $f ,true)) { // delete file
					return FALSE;
				}
			}
		}
	}
	closedir ( $handle );
	rmdir ( $filepath ); // delete directory
	return TRUE;
}

/**
 * Returns a filename based on the $name paramater that has been
 * striped of special characters, it's spaces changed to underscores,
 * and shortened to 50 characters.
 */
function _fancyfiles_sanitize_filename ($name) {
	$special_chars = array ("?", "[", "]", "/", "\\", "=", "+", "<", ">", ":", ";", ",");
	$name = str_replace ( $special_chars, "", $name );
	$name = trim ( $name, "." );
	return substr ( $name, 0, 64 );
}



/**
 * RECURSIVE READ DIRECTORY
 * for AJAX file browsing
 * Called by _fancyfiles_tree_form()
 */
function _fancyfiles_handler($nid) {
	global $user;
	$dir='';
	$output = '';
	$node = node_load ( $nid );
	isset ( $_GET ['path'] ) ? $dir = urldecode($_GET ['path']) : die();
	//if the path asked is not inside the filepath of the node, error 
	if(strpos($dir,$node->_filepath)===false) die();
	//access check
	$modify =  $node->_modify  && (user_access ( 'modify fancyfiles' ) || (user_access ( 'manage own fancyfiles' ) && ($user->uid == $node->uid)));
	$download = (user_access ( 'view fancyfiles' ) || (user_access ( 'manage own fancyfiles' ) && ($user->uid == $node->uid)));
	$tag = 'a';
	if($node->_dragdrop && node_access("update",$node)) $output.='<input type="hidden" id="fancyfiles-move-path" value="'.url('fancyfiles/move/' . $node->nid) .'" />';
	// Read files in a directory
	if($handle = opendir ( $dir )) {
		while ( false !== ($fileget = readdir ( $handle )) ) { // read directory
			$file [] = $fileget; // create file array
		}
		sort ( $file ); // sort directory array
		for($x = 0; $x < count ( $file ); $x ++) { // loop through rows of array
			if (substr ( $file [$x], 0, 1 ) != "." && $file [$x] != '_previews') {
				$s_file= $dir . '/' . $file [$x];
				if (opendir($s_file)) {
					$output .= '<div class="ff_row droppable draggable" ff_path="'.urlencode($s_file).'" ff_type="folder" '.">\n";
					$output .= '<div class="ff_data">' . "\n";
					if ($modify) {
						
						$output .= '<a href="javascript:void(0);" onclick="Fancyfiles.SubmitDelete(this);" alt="delete file" class="ff_delete">' . "&nbsp;</a>\n";
					}
					$output .= "</div>\n";
					$output .= '<a href="javascript:void(0);" onclick="Fancyfiles.ToggleFolder(this);"';
					$output .= 'class="ff_link ff_closed ">' . "\n";
					$output .= $file [$x];
					$output .= "</a>\n";
					$output .= "</div>\n";
					$output .= '<div class="container" ></div>' . "\n";
					++ $tag;
				}
				else {
					//check the existence of file in DB (case of FTP upload)
					if(!$f=_fancyfiles_fileFromPath($s_file)) {
						drupal_chmod($s_file,0664);
						$f = array(
								'filename'=>$file [$x],
								'status'=>FILE_STATUS_PERMANENT,
								'uri'=>$s_file,
								'uid'=>1,
								'filemime'=>file_get_mimetype($s_file),
						);
						$f=(object)$f;
						file_save($f);
						file_usage_add($f,"fancyfiles","node",$nid);
						$fid=$f->fid;
					}
					$output .= '<div class="ff_row draggable"  ff_path="'.urlencode($s_file).'" ff_type="file">' . "\n";
					$output .= '<div class="ff_data ">' . "\n";
					$output .= '<b>' . date ( "d/m/Y", filemtime ($s_file) ) . '</b>';
					$output .= '<i>' . _fancyfiles_resize_bytes ( filesize ($s_file) ) . '</i>';

					// allow deletion if modify was checked and access permissions are met
					if ($modify) {
						$output .= '<a href="javascript:void(0);" onclick="Fancyfiles.SubmitDelete(this);" alt="delete file" class="ff_delete">' . "&nbsp;</a>\n";
					}
					$output .= "</div>\n";
					if ($download) {
						$output .= '<a target="_blank" href="'.url("fancyfiles/download/".$node->nid.'/'.$f->fid).'">';
					}
					
					$ext = substr ($file[$x], strrpos ( $file [$x], "." )+1);
					$class=strtolower($ext);
					$output .= '<div class="ff_file '. $class . ' "> ';
					$output .= $file [$x];
					$output .="</div>";
						
					if ($download)	$output .= "</a>\n";
					$output .= '</div>' . "\n";
				}
			}
		}
		closedir ( $handle );
	}
	if ($output == '') { // Safari needs some output returned to the jahHandler
		echo "&nbsp;";
	} else {
		echo $output;
	}
}

/**
 * Returns a human readable file size
 */
function _fancyfiles_resize_bytes($size) {
	$count = 0;
	$format = array ("b", "kb", "mb", "gb", "tb" );
	while ( ($size / 1024) > 1 && $count < 4 ) {
		$size = $size / 1024;
		$count ++;
	}
	return number_format ( $size, 0, '', ',' ) . ' ' . $format [$count];
}


/**
 * Helper function for file download
 */
function _fancyfiles_download($nid,$fid) {
	global $user;
	$node = node_load ( $nid );
	if ((user_access ( 'view fancyfiles' ) || (user_access ( 'manage own fancyfiles' ) && ($user->uid == $node->uid))) && node_access ( 'view', $node )) {
		$file=file_load($fid);
		if($file) {
			$filemime=$file->filemime;
			$filename=$file->filename;
		}
		else {
			drupal_access_denied ();
		}
		$headers=array(
				"Content-Type"=> $filemime,
				"Content-Disposition"=>'attachment; filename="'.$filename.'"',
		);
		file_transfer($file->uri,$headers);
	}
	else {
		drupal_access_denied ();
		return;
	}
}


/**
 * Returns the fancyfiles folder path for the node asked
 * @param StdClass $node : node
 */
function _fancyfiles_make_path($node) {
	return file_default_scheme() . '://'.variable_get('fancyfiles_basepath','fancyfiles')."/"._fancyfiles_sanitize_filename ( $node->title ) .'_'. $node->nid;
}

/**
 * Return a file object corresponding to a uri
 * @param string $path
 * @param StdClass $node
 */
function _fancyfiles_fileFromPath($uri) {
	if(!$uri) return false;
	$query=db_select("file_managed","fu");
	$fid=$query->fields("fu",array("fid"))
	->condition("uri",$uri,"=")
	->execute()
	->fetchField();
	if($fid) {
		$file=file_load($fid);
		return $file;
	}
	return false;
}



/**
 * Checks permissions
 * used by fancyfiles_menu()
 */
function _fancyfiles_check_access($op = NULL, $arg1 = NULL, $arg2 = NULL) {
	global $user;
	switch($op) {
		case 'handler':
			if($arg1 != NULL) {
				$node = node_load ( $arg1  );
				return (user_access ( 'view fancyfiles' ) || (user_access ( 'manage own fancyfiles' ) && ($user->uid == $node->uid)));
			}
			break;
		case 'move':
			if($arg1  != NULL) {
				$node = node_load ( $arg1  );
				return ($node->_dragdrop  && node_access('update',$node));
			}
					
		case 'download':
			return user_access ( 'view fancyfiles' );
			break;
	}
	return false;
}

/**
 * Moves a files/folders within a fancyfiles folder
 * @param Int $nid
 */
function _fancyfiles_move($nid) {
	global $user;
	//access check
	if(!$node = node_load ( $nid )) return;
	if(!$node->_dragdrop && !node_access("update",$node))  return false;
	if(!isset($_GET ['source']) || !isset( $_GET ['dest'])) return false;
	
	$res=false;
	$msg="";
	$source=urldecode($_GET['source']);
	$dest=urldecode($_GET['dest']);
	if(strpos($dest,$node->_filepath)!==false || strpos($source,$node->_filepath)!==false) { //check if both source and dest are inside the node_fancyfiles->_filepath
		if($dir_dest=file_prepare_directory($dest)) { 
			if($dir_source=file_prepare_directory($source)) {
				if(strpos($dest,$source)!==false) 
					$msg=t("You can't move a folder into a subfolder within itself.");
				else {
					$res= _fancyfiles_recursive_move($source,$dest,$msg);
				}
			}
			elseif($file_source=_fancyfiles_fileFromPath($source)) {
				if($file_dest=file_move($file_source,$dest)) {
					$res=true;
				}
			}
			else {$msg=t("Internal error (wrong source).");} //wrong source
		}
		else {$msg=t("Internal error (wrong destination).");} //wrong dest
	}
	else {$msg=t("Forbidden move.");}
	
	header("Content-type: application/jsons");
	echo json_encode(array("res"=>$res,"msg"=>$msg));
}

/**
 * Recursively move files from a folder to a another
 * @param URI $dir_source
 * @param URI $dir_dest
 */
function _fancyfiles_recursive_move($uri_source,$uri_dest,&$msg) {
	$folder_dest=drupal_basename($uri_source);
	$subdest_uri=$uri_dest."/".$folder_dest;
	if(file_prepare_directory($subdest_uri)) {
		$msg="Le dossier cible existe déjà";
		return false;
	}
	if($dest=file_prepare_directory($subdest_uri,FILE_CREATE_DIRECTORY)) {
		//first, we move the file at the current depth
		foreach(file_scan_directory($uri_source,"/.+/",array("recurse"=>false)) as $uri=>$file) {
			$dir=$uri;
			if(file_prepare_directory($dir)) { //folder
				if(!_fancyfiles_recursive_move($uri,$subdest_uri,$msg)) {
					return false;
				} 
			}
			else {
				if(!file_move($file,$subdest_uri,FILE_EXISTS_REPLACE )) {
					$msg="Erreur lors du déplacement de ".drupal_basename($uri);
					return false;
				}
				else {
					file_delete(_fancyfiles_fileFromPath($uri),TRUE);
				}
			}
		}
		//recursively remove 
		if(!_fancyfiles_recursive_rmdir($uri_source)) {
			$msg="Erreur interne lors de la suppression du dossier source.";
			return false;
		};		
	}
	return true;
}
