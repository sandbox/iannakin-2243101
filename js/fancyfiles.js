(function($){
	$(function() {
		$('#startFancyfiles').next('.container').show();
		Fancyfiles.OpenFolder($('#startFancyfiles'));
		$('#-fancyfiles-uploadform').css('display', 'block');
	});
})(jQuery);

var Fancyfiles={
		dragdrop:false,
		
		ToggleFolder: function (link_folder) { //ToggleFolder: function (folder,nest) {
			(function($) {
				var row_folder=$(link_folder).parent();
				if($(row_folder).attr('ff_path')!='') {
					var target= $(row_folder).next('.container');
					if($(target).css('display')!= 'block'){
						if($(target).html()== '') {
							Fancyfiles.OpenFolder(row_folder);
						}
						$(target).show();
						$(link_folder).removeClass('ff_closed').addClass('ff_open');
					} 
					else {
						$(target).hide();
						$(link_folder).removeClass('ff_open').addClass('ff_closed');
						$(target).html('');
					}
				}
				return false;
			})(jQuery);
		},

		SubmitDelete: function (item) {
			(function($) {
				if($(item).parent().parent().attr('ff_path')!='') {
					deleteme=$(item).parent().parent().attr('ff_path');
					if (confirm('Are you sure you want to delete this item ?\r\nThis action can not be canceled.')) {
						$('#delete-uri').val(deleteme);
						$('#-fancyfiles-tree-form').get(0).submit();
					}
				}
			})(jQuery);
		},
		
		OpenFolder: function (row_folder) {
			(function($) {
				if(!$(row_folder).hasClass('ff_row')) return false;
				$(row_folder).removeClass('ff_closed').addClass('ff_open');
				path= $(row_folder).attr('ff_path');
				target=$(row_folder).next('.container');
				$(target).html('<span class="loading">&nbsp;Loading...&nbsp;</span>');
				$.ajax({
					url:$('#fancyfiles-handler-path').val(),
					data:{
						path:path,
					},
					success:function(data,textStatus,jqXHR) {
						$(target).html(data);
						Fancyfiles.ManageDragDrop();
					},
					error:function(jqXHR,textStatus,errorThrown) {
						$(target).html('');
						Fancyfiles.Error("Loading error : " +textStatus);
					}
				});
				return false;
			})(jQuery);
		},

		
		AjaxMove:function(source,dest,elem_source,elem_dest) {
			(function($) {
				if(!$('#fancyfiles-move-path').length) return;
				$.ajax({
					url:$('#fancyfiles-move-path').val(),
					data:{
						source:source,
						dest:dest
					},
					success:function(data,textStatus,jqXHR) {
						if(data.res==true) {
							$(elem_source).next('.container').remove();
							$(elem_source).remove();
							Fancyfiles.OpenFolder(elem_dest);
							return true;
						}
						else if(data.msg) {
							Fancyfiles.Error(data.msg);
							return false;
						}
					},
					error:function(jqXHR,textStatus,errorThrown) {
						Fancyfiles.Error('Error : '+errorThrown);
						return false;
					}
				});
			})(jQuery);
		},
		
		ManageDragDrop:function() {
			(function($) {
				if(!$('#fancyfiles-move-path').length) return;
				if(Fancyfiles.dragdrop) {
					$( ".draggable" ).draggable({
						revert: "invalid",
						drop:function(event,ui) {
							alert('drop event');
						}
					});
					$( ".droppable" ).droppable({
						accept: ".draggable",
						hoverClass:'active',
						drop: function( event, ui ) {
							var source=$(ui.draggable).attr('ff_path');
							var dest=$(this).attr('ff_path');
							if(confirm("Confirm the Drag'n Drop ?")) {
								if(!Fancyfiles.AjaxMove(source,dest,ui.draggable,this)) {
									$(ui.draggable).draggable( "option", "revert",true ).trigger('dragstop');
								}
							}
							else {
								$(ui.draggable).draggable( "option", "revert",true ).trigger('dragstop');
							}
						}
					});
				}
				else {
					$( ".draggable" ).draggable('destroy');
					$( ".droppable" ).droppable('destroy');
				}
			})(jQuery);
		},
		
		Error:function(msg) {
			(function($) {
				$('<div class="messages error">'+msg+'</div>').insertAfter('#tabs-wrapper');
			})(jQuery);
		},
		
		ToggleDragDrop:function() {
			(function($) {
				Fancyfiles.dragdrop=!Fancyfiles.dragdrop;
				Fancyfiles.ManageDragDrop();
				$('#ff-unlock-dragdrop a ').html((Fancyfiles.dragdrop?"Disable":"Enable")+"  Drag & Drop");
				if(Fancyfiles.dragdrop)	{
					$('#startFancyfiles').removeClass('noDnD');
					$('#ff-unlock-dragdrop').removeClass('locked').addClass('unlocked');
				}
				else {
					$('#startFancyfiles').addClass('noDnD');
					$('#ff-unlock-dragdrop').removeClass('unlocked').addClass('locked');
				}
			})(jQuery);
		}

}


